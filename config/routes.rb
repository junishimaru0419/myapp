# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }
  root 'top_pages#home'
  get '/about' => 'top_pages#about'
  resources :users, only: [:show]
  resources :proposals, only: %i[show create destroy] do
    resources :reviews, only: %i[create destroy]
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
