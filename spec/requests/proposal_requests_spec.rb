require 'rails_helper'

RSpec.describe 'Proposals', type: :request do
  describe 'Proposals#create' do
    let(:proposal) { FactoryBot.attributes_for(:proposal) }
    let(:post_request) { post proposals_path, params: { proposal: proposal } }

    context 'when not logged in' do
      it "doesn't change Proposal's count" do
        expect { post_request }.to change(Proposal, :count).by(0)
      end

      it 'redirects to login_url' do
        expect(post_request).to redirect_to new_user_session_url
      end
    end
  end

  describe 'Proposals#destroy' do
    let!(:proposal) { FactoryBot.create(:proposal) }
    let(:delete_request) { delete proposal_path(proposal) }

    context 'when not logged in' do
      it "doesn't change Proposal's count" do
        expect { delete_request }.to change(Proposal, :count).by(0)
      end

      it 'redirects to login_url' do
        expect(delete_request).to redirect_to new_user_session_url
      end
    end

    # 間違ったユーザによるマイクロポスト削除に対してテスト
    context "when logged in user tyies to delete another user's proposal" do
      let(:user) { FactoryBot.create(:user) }

      before { login_as(user) }

      it "doesn't change Proposal's count" do
        expect { delete_request }.to change(Proposal, :count).by(0)
      end
      it 'redirects to login_url' do
        expect(delete_request).to redirect_to root_url
      end
    end
  end
end
