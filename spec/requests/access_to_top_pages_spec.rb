require 'rails_helper'

RSpec.describe 'Access to top_pages', type: :request do
  context 'GET #home' do
    before { get root_path }
    it 'responds successfully' do
      expect(response).to have_http_status 200
    end
    it "has title 'My Proposal'" do
      expect(response.body).to include full_title('')
      expect(response.body).to_not include '| My Proposal'
    end
  end

  context 'GET #about' do
    before { get about_path }
    it 'responds successfully' do
      expect(response).to have_http_status 200
    end
    it "has title 'Home | My Proposal'" do
      expect(response.body).to include full_title('My Proposalとは')
    end
  end
end
