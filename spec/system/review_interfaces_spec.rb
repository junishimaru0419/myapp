require 'rails_helper'

RSpec.describe 'ReviewInterfaces', type: :system do
  let(:user) { FactoryBot.create(:user) }
  let!(:proposal) { FactoryBot.create(:proposal) }

  describe '#create,#destroy' do
    context 'レビュー行う' do
      it 'ユーザーが投稿に対してレビューができる' do
        # ログインする
        visit new_user_session_url
        find('input[type="email"]').set(user.email)
        find('input[type="password"]').set(user.password)
        click_button 'ログイン'
        expect(page).to have_content 'ログインしました'

        click_on 'ホーム'

        # レビューのページへ遷移する
        visit proposal_path(proposal)

        # 評価の星が表示されていることを確認する
        expect(page).to have_css '.star-form-group'

        # レビューボタンをクリック
        click_on 'レビューを投稿する'

        # レビューページへ戻されることを確認する
        expect(current_path).to eq(proposal_path(proposal))
      end
    end

    context 'レビューを削除する' do
      it 'ユーザーが投稿したレビューを削除できる' do
        # 投稿を削除する
        expect do
          page.accept_confirm do
            all('ol li')[0].click_on '削除'
            delete proposal_reviews_path(proposal)
          end
        end
      end
    end
  end
end
