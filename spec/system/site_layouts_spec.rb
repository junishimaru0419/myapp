require 'rails_helper'

RSpec.describe 'site layout', type: :system do
  context 'access to root_path' do
    before { visit root_path }
    subject { page }
    it 'has links sach as root_path, about_path' do
      is_expected.to have_link nil, href: root_path
      is_expected.to have_link 'My Proposalとは', href: about_path
    end
  end
end
