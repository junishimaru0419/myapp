require 'rails_helper'

RSpec.describe 'ProposalsInterfaces', type: :system do
  let(:user) { FactoryBot.create(:user) }
  let!(:proposal) { user.proposals.create(content: 'This is test post') }

  scenario 'proposal interface' do
    visit new_user_session_url
    find('input[type="email"]').set(user.email)
    find('input[type="password"]').set(user.password)
    click_button 'ログイン'
    expect(page).to have_content 'ログインしました'

    click_on 'ホーム'

    # 無効な送信
    click_on '投稿'
    expect(has_css?('.alert-danger')).to be_truthy

    # 有効な送信
    valid_content = 'This proposal really ties the room together'
    fill_in 'proposal_content', with: valid_content
    attach_file 'proposal[file]', "#{Rails.root}/spec/fixtures/files/sample.pdf"
    expect do
      click_on '投稿'
      post proposals_path, params: { proposal: { content: 'This is second post' } }
    end

    # 投稿を削除する
    expect do
      page.accept_confirm do
        all('ol li')[0].click_on '削除'
        delete proposal_path(proposal)
      end
    end

    # 違うユーザのプロフィールにアクセス(削除リンクがないことを確認)
    visit user_path(proposal.user)
    expect(page).not_to have_link '削除'
  end
end
