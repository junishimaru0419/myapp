require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    it { expect(full_title('')).to eq 'My Proposal' }
    it { expect(full_title('My Proposalとは')).to eq 'My Proposalとは | My Proposal' }
  end
end
