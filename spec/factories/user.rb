FactoryBot.define do
  factory :user do
    username              { 'Jhon' }
    sequence(:email)      { |n| "tester#{n}@example.com" }
    password              { '111111' }
    password_confirmation { '111111' }
    after(:create) { |user| user.confirm }
  end
end
