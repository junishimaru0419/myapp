FactoryBot.define do
  factory :review do
    rate { '5' }
    association :user
    association :proposal
  end
end