FactoryBot.define do
  factory :proposal do
    content { 'proposal test' }
    file { File.open(File.join(Rails.root, 'spec/fixtures/files/sample.pdf')) }
    created_at { 10.minutes.ago }
    association :user

    # proposalモデルの順序付けテストデータを追加
    trait :yesterday do
      content { 'yesterday' }
      created_at { 1.day.ago }
    end

    trait :day_before_yesterday do
      content { 'day_before_yesterday' }
      created_at { 2.days.ago }
    end

    trait :now do
      content { 'now!' }
      created_at { Time.zone.now }
    end
  end
end
