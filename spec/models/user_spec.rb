require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryBot.build(:user) }

  describe 'ユーザー登録' do
    it 'name、email、passwordとpassword_confirmationが存在すれば登録できること' do
      user = build(:user)
      expect(user).to be_valid
    end
  end

  # "dependent: :destroy"投稿と削除のテスト
  describe 'dependent: :destroy' do
    before do
      user.save
      user.proposals.create!(content: 'Lorem ipsum', file: File.open(File.join(Rails.root, 'spec/fixtures/files/sample.pdf')))
    end

    it 'succeeds' do
      expect do
        user.destroy
      end.to change(Proposal, :count).by(-1)
    end
  end
end
