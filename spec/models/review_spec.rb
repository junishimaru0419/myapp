require 'rails_helper'

RSpec.describe Review, type: :model do
  before do
    @review = FactoryBot.build(:review)
  end

  describe '#create' do
    context 'レビューを投稿できる場合' do
      it '評価入力済みあれば投稿できる' do
        expect(@review).to be_valid
      end
    end

    context '投稿できない場合' do
      it '評価が空では投稿できない' do
        @review.rate = ''
        @review.valid?
        expect(@review.errors.full_messages).to include('Rateを入力してください')
      end
    end
  end
end
