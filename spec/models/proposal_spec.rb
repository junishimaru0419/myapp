require 'rails_helper'

RSpec.describe Proposal, type: :model do
  # associationで関連付け
  let(:user) { FactoryBot.create(:user) }
  let(:proposal) { FactoryBot.create(:proposal, user_id: user.id) }

  # テストデータが有効かどうか
  it "is valid with proposal's test data" do
    expect(proposal).to be_valid
  end

  # user_idが無いと無効
  it 'is invalid with no user_id' do
    proposal.user_id = nil
    expect(proposal).to be_invalid
  end

  # contentが空白だと無効
  it 'is invalid with no title' do
    proposal.content = ' '
    expect(proposal).to be_invalid
  end

  # contentが41文字以上だと無効
  it 'is invalid with 41 characters or more' do
    proposal.content = 'a' * 41
    expect(proposal).to be_invalid
  end

  # Proposalモデルの順序付けのテスト
  describe 'Sort by latest' do
    # 評価される前にdbに保存されていないといけないので、「let!」を使用
    let!(:day_before_yesterday) { FactoryBot.create(:proposal, :day_before_yesterday) }
    # FactoryBot.create(:proposal, :now) を一番上に持ってくるとテストの意味がなくなる
    let!(:now) { FactoryBot.create(:proposal, :now) }
    let!(:yesterday) { FactoryBot.create(:proposal, :yesterday) }

    it 'succeeds' do
      expect(Proposal.first).to eq now
    end
  end
end
