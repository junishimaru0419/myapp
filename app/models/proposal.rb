class Proposal < ApplicationRecord
  belongs_to :user
  has_many :reviews, dependent: :destroy
  default_scope -> { order(created_at: :desc) }
  mount_uploader :file, FileUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 40 }
  validates :file, presence: true
  validate  :file_size

  private

  # アップロードされた画像のサイズをバリデーションする
  def file_size
    errors.add(:picture, '15MB未満である必要があります') if file.size > 15.megabytes
  end
end
