class ProposalsController < ApplicationController
  before_action :authenticate_user!, only: %i[show create destroy]
  before_action :correct_user, only: :destroy

  def show
    @proposal = Proposal.find(params[:id])
    @review = Review.new
    @reviews = @proposal.reviews.order(created_at: :desc)
  end

  def create
    @proposal = current_user.proposals.build(proposal_params)
    if @proposal.save
      flash[:success] = 'ファイルをアップロードしました'
      redirect_to root_url
    else
      @feed_items = []
      render 'top_pages/home'
    end
  end

  def destroy
    @proposal.destroy
    flash[:success] = 'ファイルを削除しました'
    redirect_to request.referrer || root_url
  end

  private

  def proposal_params
    params.require(:proposal).permit(:content, :file)
  end

  def correct_user
    @proposal = current_user.proposals.find_by(id: params[:id])
    redirect_to root_url if @proposal.nil?
  end
end
