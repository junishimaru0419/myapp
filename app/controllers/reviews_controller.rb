class ReviewsController < ApplicationController
  before_action :authenticate_user!, only: %i[create destroy]
  before_action :correct_user, only: :destroy

  def create
    @proposal = Proposal.find(params[:proposal_id])
    @review = Review.new(review_params)
    flash[:success] = if @review.save
                        'レビューしました'
                      else
                        '評価をつけてください。'
                      end
    redirect_back(fallback_location: root_path)
  end

  def destroy
    @review.destroy
    flash[:success] = 'レビューを削除しました'
    redirect_back(fallback_location: root_path)
  end

  private

  def review_params
    params.require(:review).permit(:text, :rate).merge(user_id: current_user.id, proposal_id: params[:proposal_id])
  end

  def correct_user
    @review = current_user.reviews.find_by(id: params[:id])
    redirect_to root_url if @review.nil?
  end
end
