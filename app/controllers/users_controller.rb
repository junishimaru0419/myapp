class UsersController < ApplicationController
  def show
    @current_user = User.find(params[:id])
    @proposals = @current_user.proposals.page(params[:page])
    @review = Review.new
  end
end
