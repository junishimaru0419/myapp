class OmniauthCallbacksController < ApplicationController
  def twitter
    @user = User.from_omniauth(request.env['omniauth.auth'].except('extra'))

    if @user.persisted?
      sign_in_and_redirect @user, event: :authentication
    else
      @user.skip_confirmation!
      @user.save!
      sign_in_and_redirect @user
    end
  end
end
