# frozen_string_literal: true

class TopPagesController < ApplicationController
  before_action :sign_in_required, only: [:show]

  def home
    return unless user_signed_in?

    @proposal = current_user.proposals.build
    @all_items = Proposal.all.page(params[:page])
  end

  def about; end
end
