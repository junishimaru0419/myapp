# README

## タイトル  
My Proposal

## URL  
https://myproposal-20210904065754.herokuapp.com/

## 概要  
自分で考えた企画書をWEB上でお互いに評価し合うというサイトです。

* 自分の企画が面白いかどうか色んな人に評価して欲しい  
* 今自分の企画書に何が足りないかコメントして欲しい  
* 他の人がどんな企画を考えているのか色々見て参考にしたい　など  

学生、社会人問わず、様々な意見が飛び交うサイトとなっています。

![Scheme](https://bitbucket.org/junishimaru0419/myapp/raw/cb97d91ba7bf8fb1911198d822b409f51fb1e039/app/assets/images/overview.png)

## 使用技術  
* Ruby 2.6.5  
* Ruby on Rails 5.2.6  
* Docker/Docker-compose    
* CircleCI CI/CD  
* Rubocop  
* Rspec

##　インフラ構成図  
![Scheme](https://bitbucket.org/junishimaru0419/myapp/raw/cb97d91ba7bf8fb1911198d822b409f51fb1e039/app/assets/images/composition.png)

### CircleCI
・Bitbucketへのpush時に、Rubocopが自動で実行されます。
・Rubocopが成功した場合成功した場合、herokuへの自動デプロイが実行されます。

## 機能一覧  
* ユーザー登録、ログイン機能(devise)  
* Twitter認証機能(omniauth-twitter)  
* PDF投稿機能(carrierwave)  
* ページネーション機能(kaminari)
* レビュー機能(jquery-rails)

## テスト  
* Rspec  
  ・単体テスト(model)  
  ・機能テスト(request)    
  ・統合テスト(system)
