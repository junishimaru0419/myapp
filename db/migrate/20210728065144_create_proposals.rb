class CreateProposals < ActiveRecord::Migration[5.2]
  def change
    create_table :proposals do |t|
      t.text :content
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :proposals, [:user_id, :created_at]
  end
end
