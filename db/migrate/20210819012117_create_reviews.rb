class CreateReviews < ActiveRecord::Migration[5.2]
  def change
    create_table :reviews do |t|
      t.integer :user_id, null: false
      t.integer :proposal_id, null: false
      t.float :rate, null: false, default: 0
      t.text :text, null: false
      
      t.timestamps
    end
  end
end
